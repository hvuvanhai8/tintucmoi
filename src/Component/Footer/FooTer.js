import React, { Component } from 'react';

class FooTer extends Component {
    render() {
        return (
            <footer style={{marginTop: '80px'}}>
            <div className="container" style={{marginBottom: 0}}>
              <div className="row">
                <div className="col-md-4">
                  <div className="ft_a">
                    <h6>Blog thông tin điện tử tổng hợp zing 24h</h6>
                    <p>Ghi rõ nguồn hanoi24h.net khi sử dụng lại thông tin từ website này </p>
                    <p>Zing 24h tổng hợp và sắp xếp các thông tin tự động </p>
                    <p>bởi chương trình máy tính</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="ft_b">
                    <ul>
                      <li><a href="#">Liên hệ</a></li>
                      <li><a href="#">Giới thiệu</a></li>
                      <li><a href="#">Điều khoản sử dụng</a></li>
                      <li><a href="#">Quảng cáo</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-4">
                </div>
              </div>
            </div>
          </footer>
        );
    }
}

export default FooTer;