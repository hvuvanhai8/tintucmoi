import React, { Component } from 'react';

class DanhChoBan extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        items: [],
        isLoader: false,
       }
    }

    componentDidMount() {

      fetch('https://sosanhnha.com/api/v2/classifieds')
        .then(res => res.json())
        .then(json => {
          this.setState({
            isLoader: true,
            items: json,
          })
        });

    }

    render() {

        var { isLoader,items } = this.state;

        if (!isLoader) {
          return <div>Loading...</div>;
        }

        else{
          console.log(items);
          return (
                    <section>
                    <div className="container">
                      <div className="list_home">
                        <div className="row">
                          <div className="col-md-9">
                            <h4>Dành cho bạn</h4>
                            
                            {items.data.classifieds.map(item => (
                              <div className="row">
                                <div className="news_list d-flex">
                                  <div><img src={item.picture} className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                                  <div><a href="#" className="nav-link txt-a"> {item.title} </a><time className="nav-link">{item.date}</time></div>
                                </div> 
                              </div>
                            ))};
                            

                          </div>
                          <div className="col-md-3" />
                        </div>
                      </div> 
                    </div>
                  </section>
          );
        }
        
    }
}

export default DanhChoBan;