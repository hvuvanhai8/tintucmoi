import React, { Component } from 'react';

class TinMoi extends Component {
    render() {
        return (
            <section>
              <div className="container" style={{marginTop: '80px'}}>
                <div className="top_home">
                  <div className="row">
                    <div className="col-md-12">
                      <h5>Tin mới</h5>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-3">
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_list d-flex">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="highlights_home">
                        <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                        <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        <div>Nếu khó quá thì thuê Phó thủ tướng xuống, tôi làm cho. Tôi cũng biết chút chút về kế toán", Phó thủ tướng Vương Đình Huệ nói khi đại diện hải quan không trả lời cụ thể cho DN.</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="row">
                        <div className="news_hot_right">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="news_hot_right">
                          <div><img src="images/dammeduongsat_thumb.jpg" className="rounded" alt="Cinque Terre" width="100%" height="100%" /></div>
                          <div><a href="#" className="nav-link txt-a"> Tàu trật bánh tại Nam Định làm tê liệt đường sắt Bắc Nam</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </section>
        );
    }
}

export default TinMoi;