import React from 'react';

import './App.css';
import TopMenu from './Component/TopMenu/TopMenu';
import TinMoi from './Component/TinMoi/TinMoi';
import DanhChoBan from './Component/DanhChoBan/DanhChoBan';
import FooTer from './Component/Footer/FooTer';


function App() {
  return (
    <div className="tintuc">
      <TopMenu/>
      <TinMoi/>
      <DanhChoBan title="Cô gái bị sàm sỡ trên xe Phương Trang: 'Tôi vẫn chưa hết bàng hoàng" title1="Điện thoại thương hiệu Việt và nỗi đau gục ngã ngay trên sân nhà" image="https://cdn.tuoitre.vn/thumb_w/586/2019/6/7/u22-15598756846161724462609.jpg" image1="https://znews-photo.zadn.vn/w860/Uploaded/Aohuouk/2017_08_11/Bphone2017_Zing.jpg"/>
      <FooTer/>
    </div>
  );
}

export default App;
